﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// 有关程序集的常规信息通过以下
// 特性集控制。更改这些特性值可修改
// 与程序集关联的信息。
[assembly: AssemblyTitle("TC.Vacation.Spider")]
[assembly: AssemblyDescription("分布式网页数据抓取组件")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("同程网络科技股份有限公司")]
[assembly: AssemblyProduct("TC.Vacation.Spider")]
[assembly: AssemblyCopyright("Copyright © 同程网络科技股份有限公司")]
[assembly: AssemblyTrademark("同程旅游")]
[assembly: AssemblyCulture("")]

// 将 ComVisible 设置为 false 使此程序集中的类型
// 对 COM 组件不可见。  如果需要从 COM 访问此程序集中的类型，
// 则将该类型上的 ComVisible 特性设置为 true。
[assembly: ComVisible(false)]

// 如果此项目向 COM 公开，则下列 GUID 用于类型库的 ID
[assembly: Guid("b5dacd5c-0e18-416d-86f4-94bd9faa4a67")]

// 程序集的版本信息由下面四个值组成: 
//
//      主版本
//      次版本 
//      生成号
//      修订号
//
// 可以指定所有这些值，也可以使用“生成号”和“修订号”的默认值，
// 方法是按如下所示使用“*”: 
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2.0.2.01")]
[assembly: AssemblyFileVersion("2.0.2.01")]
