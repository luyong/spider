﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TC.Vacation.Spider.Events
{
    public class OnReveivedEventArgs:EventArgs
    {
        /// <summary>
        /// 从客户端接收到的信息
        /// </summary>
        /// <value>
        /// The message.
        /// </value>
        public string Message { get; internal set; }
    }
    
    /// <summary>
    /// 从客户端接收到的信息时的事件驱动
    /// </summary>
    /// <param name="args">The <see cref="TC.Vacation.Spider.Events.OnReveivedEventArgs"/> instance containing the event data.</param>
    public delegate void OnReveivedEventHandler(OnReveivedEventArgs args);
}
