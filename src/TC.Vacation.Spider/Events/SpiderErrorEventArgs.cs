﻿using System;

namespace TC.Vacation.Spider.Events
{
    /// <summary>
    /// 爬取错误
    /// </summary>
    public class SpiderErrorEventArgs
    {
        /// <summary>
        /// 爬取的Url--只读属性
        /// </summary>
        public string Url { get; internal set; }
        /// <summary>
        /// 异常信息--只读属性
        /// </summary>
        public Exception Exception { get; internal set; }
        /// <summary>
        /// 错误信息--只读属性
        /// </summary>
        public string Error { get; internal set; }
        /// <summary>
        /// 抓取耗时
        /// </summary>
        /// <value>
        /// The spent.
        /// </value>
        public double Spent { get; internal set; }
        /// <summary>
        /// 错误时返回的HTML源码
        /// </summary>
        /// <value>
        /// The error HTML.
        /// </value>
        public string ErrorHtml { get; internal set; }
    }
}
