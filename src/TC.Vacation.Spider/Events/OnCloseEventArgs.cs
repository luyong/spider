﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TC.Vacation.Spider.Events
{
    public class OnCloseEventArgs:EventArgs
    {
        public int Connection { get; internal set; }
    }

    public delegate void OnCloseEventHandler(OnCloseEventArgs args);
}
