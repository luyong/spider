﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TC.Vacation.Spider.Events
{
    public class OnConectionEventArgs : EventArgs
    {
        /// <summary>
        /// 当前连接数
        /// </summary>
        /// <value>
        /// The connection.
        /// </value>
        public int Connection { get; internal set; }
    }

    /// <summary>
    /// 当客户端连接到Master时的事件驱动
    /// </summary>
    /// <param name="args">The <see cref="OnConectionEventArgs"/> instance containing the event data.</param>
    public delegate void OnConectionEventHandler(OnConectionEventArgs args);
}
