﻿using System;

namespace TC.Vacation.Spider.Events
{
    public class DataReceivedEventArgs
    {
        /// <summary>
        /// 当前url--只读属性
        /// </summary>
        public string Url { get; internal set; }
        /// <summary>
        /// 当前获取到的Html--只读属性
        /// </summary>
        public string Html { get; internal set; }
        /// <summary>
        /// Http状态--只读属性
        /// </summary>
        public int HttpStatus { get; internal set; }
        /// <summary>
        /// 抓取耗时
        /// </summary>
        /// <value>
        /// The spent.
        /// </value>
        public double Spent { get; internal set; }
        /// <summary>
        /// 当在继承RulesAnalyzer后在派生类中重写Filter方法时，在DynamicEntity可以获取返回值
        /// </summary>
        /// <value>
        /// The dynamic entity.
        /// </value>
        public dynamic DynamicEntity { get; internal set; }
        /// <summary>
        /// Http状态描述信息
        /// </summary>
        /// <value>
        /// The HTTP status description.
        /// </value>
        public string HttpStatusDescription { get; internal set; }

        /// <summary>
        /// 当前请求数（第几个请求、URL）
        /// </summary>
        public int Tick { get; internal set; }
    }
}
