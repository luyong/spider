﻿using System.Net;

namespace TC.Vacation.Spider.Loggers
{
    /// <summary>
    /// 统一日志
    /// </summary>
    internal sealed class Logger
    {
        /// <summary>
        /// The post url_ information
        /// </summary>
        const string PostUrl_Info = "http://tcservice.17usoft.com/vacations/workplatform/Logger/Info";
        /// <summary>
        /// The post url_ error
        /// </summary>
        const string PostUrl_Error = "http://tcservice.17usoft.com/vacations/workplatform/Logger/Error";

        /// <summary>
        /// Writes the specified ext.
        /// </summary>
        /// <param name="ext">The ext.</param>
        internal static void Write(string ext)
        {
            string url = string.Format("{0}?ext={1}", PostUrl_Info, ext);
            Post(url);
        }

        /// <summary>
        /// Writes the specified ext.
        /// </summary>
        /// <param name="ext">The ext.</param>
        /// <param name="error">The error.</param>
        internal static void Write(string ext, string error)
        {
            string url = string.Format("{0}?ext={1}&error={2}", PostUrl_Error, ext, error);
            Post(url);
        }

        /// <summary>
        /// Posts the specified URL.
        /// </summary>
        /// <param name="url">The URL.</param>
        private static void Post(string url)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.BeginGetResponse(null, null);
            }
            finally { }
        }
    }
}
