﻿using System.IO;
using HttpWebAdapters;

namespace TC.Vacation.Spider.Web
{
    internal class WebRequestState
    {
        const int BufferSize = 1024;

        /// <summary>
        /// 每次写入的字节
        /// </summary>

        public byte[] BufferRead;

        /// <summary>
        /// WebRequest
        /// </summary>

        public IHttpWebRequest Request;

        /// <summary>
        /// 响应流
        /// </summary>

        public Stream ResponseStreamAsync;


        /// <summary>
        /// 异步请求
        /// </summary>

        public AsyncWebRequest RequestInfo;

        /// <summary>
        /// 默认构造函数，初始化部分信息
        /// </summary>
        /// <param name="requestInfo"></param>
        public WebRequestState(AsyncWebRequest requestInfo)
        {
            BufferRead = new byte[BufferSize];
            Request = requestInfo.Request;
            ResponseStreamAsync = null;
            RequestInfo = requestInfo;
        }
    }
}
