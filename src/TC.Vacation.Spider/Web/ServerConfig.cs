﻿using System;
using System.Text;
using System.Web;
using HttpWebAdapters;

namespace TC.Vacation.Spider.Web
{
    public sealed class ServerConfig
    {
        /// <summary>
        /// 默认配置
        /// </summary>
        static ServerConfig()
        {
            _Default = new ServerConfig
            {
                Timeout = TimeSpan.FromSeconds(10),
                UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 UBrowser/3.1.1644.34 Safari/537.36"
            };
        }

        private static ServerConfig _Default;

        /// <summary>
        /// 获取默认的配置信息
        /// </summary>
        public static ServerConfig Default
        {
            get
            {
                return _Default;
            }
        }

        /// <summary>
        /// 合并配置信息
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        public static ServerConfig Merge(ServerConfig options)
        {
            ServerConfig config = ServerConfig.Default;
            if (options != null)
            {
                if (options.UserAgent != null)
                {
                    config.UserAgent = options.UserAgent;
                }
                if (options.Timeout != null)
                {
                    config.Timeout = options.Timeout;
                }
                if (options.Address != null)
                {
                    config.Address = options.Address;
                }
                if (options.Port != 0)
                {
                    config.Port = options.Port;
                }
                config.Mehtod = options.Mehtod;
                config.KeepAlive = options.KeepAlive;
                config.UseProxy = options.UseProxy;
            }
            return config;
        }

        /// <summary>
        /// 将配置信息应用到请求上
        /// </summary>
        /// <param name="options"></param>
        /// <param name="request"></param>
        public static void Apply(ServerConfig options, ICsqWebRequest request)
        {
            var opts = Merge(options);
            if (opts.Timeout != null)
            {
                request.Timeout = (int)Math.Floor(opts.Timeout.TotalMilliseconds);
            }
            if (opts.UserAgent != null)
            {
                request.UserAgent = opts.UserAgent;
            }
        }

        /// <summary>
        /// 是否启用代理
        /// </summary>
        public bool UseProxy { get; set; }

        /// <summary>
        /// 获取设置浏览器标识
        /// </summary>
        public string UserAgent { get; set; }

        /// <summary>
        /// 超时
        /// </summary>
        public TimeSpan Timeout { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        public Encoding Encoding { get; set; }

        /// <summary>
        /// 请求方式
        /// </summary>
        public HttpWebRequestMethod Mehtod { get; set; }

        /// <summary>
        /// 响应类型
        /// </summary>
        public string ContentType { get; set; }

        /// <summary>
        /// 保持连接
        /// </summary>
        public bool KeepAlive { get; set; }

        /// <summary>
        /// 代理地址
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 代理端口
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// 是否异步
        /// </summary>
        public bool Async { get; set; }

        /// <summary>
        /// 设置超时--秒
        /// </summary>
        public double TimeoutSeconds
        {

            get
            {
                return Timeout == null ? 0 : Timeout.TotalSeconds;
            }
            set
            {
                Timeout = TimeSpan.FromSeconds(value);
            }
        }
    }
}
