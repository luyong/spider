﻿
namespace TC.Vacation.Spider.Web
{
    /// <summary>
    /// 请求状态
    /// </summary>
    public enum RequestState
    {
        Idle = 1,

        Active = 2,

        Fail = 3,

        PartialSuccess = 4,

        Success = 5
    }
}
