﻿using System;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Text;
using System.Threading;
using HttpWebAdapters;
using HttpWebAdapters.Adapters;

namespace TC.Vacation.Spider.Web
{
    /// <summary>
    /// 异步web请求
    /// </summary>

    internal class AsyncWebRequest : ICsqWebResponse
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AsyncWebRequest"/> class.
        /// </summary>
        /// <param name="request">The request.</param>
        public AsyncWebRequest(IHttpWebRequest request)
        {
            Request = request;
            Timeout = request.Timeout;
            UserAgent = request.UserAgent;
        }

        /// <summary>
        /// The buffe r_ size
        /// </summary>
        const int BUFFER_SIZE = 1024;

        /// <summary>
        /// 异步请求信号量
        /// </summary>

        protected ManualResetEvent allDone = new ManualResetEvent(false);

        /// <summary>
        /// html流
        /// </summary>

        protected Stream ResponseStream { get; set; }

        /// <summary>
        /// 设置或获取编码
        /// </summary>

        public Encoding ResponseEncoding { get; set; }

        /// <summary>
        /// Cookie保持对象
        /// </summary>
        private static CookieContainer CookieContainer;

        /// <summary>
        /// 请求时出现的异常
        /// </summary>
        private WebException _WebException;

        #region public properties

        /// <summary>
        /// 成功后的回调函数
        /// </summary>
        public Action<ICsqWebResponse> CallbackSuccess { get; set; }

        /// <summary>
        /// 失败后的回调函数
        /// </summary>
        public Action<ICsqWebResponse> CallbackFail { get; set; }

        /// <summary>
        /// 请求唯一标识ID
        /// </summary>
        public object Id { get; set; }

        /// <summary>
        /// 请求的url
        /// </summary>
        public string Url
        {
            get
            {
                return Request.RequestUri.AbsoluteUri;
            }
        }

        /// <summary>
        /// 异步请求开始时间
        /// </summary>
        public DateTime? Started { get; protected set; }

        /// <summary>
        /// 异步请求完成时间
        /// </summary>
        public DateTime? Finished { get; protected set; }

        /// <summary>
        /// 标识异步请求完成
        /// </summary>
        public bool Complete
        {
            get
            {
                return Finished != null;
            }
            protected set
            {
                if (value == true)
                {
                    Finished = DateTime.Now;
                }
                else
                {
                    throw new InvalidOperationException("You can only set complete to True.");
                }
            }
        }

        /// <summary>
        /// 获取异步请求是否成
        /// </summary>
        public bool Success
        {
            get;
            protected set;
        }

        /// <summary>
        /// 请求时出现的异常
        /// </summary>
        public WebException WebException
        {
            get
            {
                return _WebException;
            }
            protected set
            {
                _WebException = value;
            }
        }

        /// <summary>
        /// 请求对象
        /// </summary>

        public IHttpWebRequest Request
        {
            get;
            set;
        }

        /// <summary>
        /// 请求出现的错误信息
        /// </summary>

        public string Error
        {
            get
            {
                if (WebException == null)
                {
                    return "";
                }
                else
                {
                    return WebException.Message;
                }
            }
        }

        /// <summary>
        /// http状态码
        /// </summary>

        public int HttpStatus
        {
            get
            {

                if (Response == null)
                {
                    return 0;
                }
                else
                {
                    return (int)Response.StatusCode;
                }
            }
        }

        /// <summary>
        /// http状态文字说明
        /// </summary>

        public string HttpStatusDescription
        {
            get
            {
                if (Response == null)
                {
                    return "";
                }
                else
                {
                    return Response.StatusDescription;
                }
            }
        }

        /// <summary>
        /// 一个异步响应对象
        /// </summary>

        public IHttpWebResponse Response
        {
            get;
            protected set;
        }

        /// <summary>
        /// 请求返回的HTML源码
        /// </summary>

        public string Html
        {
            get
            {
                ResponseStream.Position = 0;
                using (var reader = new StreamReader(ResponseStream, ResponseEncoding))
                {
                    return reader.ReadToEnd();
                }
            }
        }

        /// <summary>
        /// 开始异步请求
        /// </summary>
        /// <returns></returns>
        public ManualResetEvent GetAsync()
        {
            Started = DateTime.Now;

            WebRequestState rs = new WebRequestState(this);

            rs.Request = Request;

            IAsyncResult r = (IAsyncResult)Request.BeginGetResponse(
               new AsyncCallback(RespCallback), rs);
            return allDone;
        }


        #endregion

        #region 私有方法

        /// <summary>
        /// Resps the callback.
        /// </summary>
        /// <param name="ar">The ar.</param>
        private void RespCallback(IAsyncResult ar)
        {
            WebRequestState rs = (WebRequestState)ar.AsyncState;

            IHttpWebRequest req = rs.Request;
            req.Timeout = Timeout;
            req.Headers["UserAgent"] = UserAgent;
            req.CookieContainer = Cookie;

            try
            {
                Response = (IHttpWebResponse)req.EndGetResponse(ar);
            }
            catch (WebException e)
            {
                Complete = true;
                Success = false;
                WebException = e;

                if (CallbackFail != null)
                {
                    CallbackFail(this);
                }
                allDone.Set();
                return;
            }

            ResponseEncoding = CsqWebRequest.GetEncoding(Response) ?? Options.Encoding;

            Stream responseStream = Response.ContentEncoding == "gzip" ?
                        new GZipStream(Response.GetResponseStream(), CompressionMode.Decompress) :
                        Response.GetResponseStream();

            rs.ResponseStreamAsync = responseStream;
            ResponseStream = new MemoryStream();

            IAsyncResult iarRead = responseStream.BeginRead(rs.BufferRead, 0,
               BUFFER_SIZE, new AsyncCallback(ReadCallBack), rs);
        }


        /// <summary>
        /// Reads the call back.
        /// </summary>
        /// <param name="asyncResult">The asynchronous result.</param>
        private void ReadCallBack(IAsyncResult asyncResult)
        {
            Encoding encoding = Encoding.UTF8;

            WebRequestState rs = (WebRequestState)asyncResult.AsyncState;

            Stream responseStream = rs.ResponseStreamAsync;

            int read = responseStream.EndRead(asyncResult);
            if (read > 0)
            {
                ResponseStream.Write(rs.BufferRead, 0, read);

                IAsyncResult ar = responseStream.BeginRead(
                   rs.BufferRead, 0, BUFFER_SIZE,
                   new AsyncCallback(ReadCallBack), rs);
            }
            else
            {
                if (ResponseStream.Length > 0)
                {
                    ResponseStream.Position = 0;
                    Complete = true;
                    Success = true;

                    if (CallbackSuccess != null)
                    {
                        CallbackSuccess(this);
                    }
                }
                else
                {
                    Complete = true;
                    Success = false;
                    if (CallbackFail != null)
                    {
                        CallbackFail(this);
                    }
                }
                responseStream.Close();

                allDone.Set();
            }
            return;
        }

        #endregion


        /// <summary>
        /// 请求配置项
        /// </summary>
        /// <exception cref="System.NotImplementedException">
        /// </exception>
        public ServerConfig Options
        {
            get;
            set;
        }

        /// <summary>
        /// 设置获取超时时间
        /// </summary>

        public int Timeout
        {
            get;
            set;
        }

        /// <summary>
        /// 设置获取UserAgent
        /// </summary>

        public string UserAgent
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the web proxy.
        /// </summary>
        /// <value>
        /// The web proxy.
        /// </value>
        public IWebProxy WebProxy
        {
            get
            {
                return Request.Proxy;
            }
        }

        /// <summary>
        /// 抓取耗时
        /// </summary>
        /// <value>
        /// The spent.
        /// </value>
        public double Spent
        {
            get
            {
                TimeSpan? time = Finished - Started;
                return time.HasValue ? time.Value.TotalMilliseconds : 0;
            }
        }

        /// <summary>
        /// Gets the cookie.
        /// </summary>
        /// <value>
        /// The cookie.
        /// </value>
        public CookieContainer Cookie
        {
            get
            {
                if (Response != null && CookieContainer == null)
                {
                    CookieContainer = new System.Net.CookieContainer();
                    string cookiesExpression = Response.Headers["Set-Cookie"];
                    if (!string.IsNullOrEmpty(cookiesExpression))
                    {
                        Uri cookieUrl = new Uri(string.Format("{0}://{1}:{2}/",
                            Response.ResponseUri.Scheme,
                            Response.ResponseUri.Host,
                            Response.ResponseUri.Port));
                        CookieContainer.SetCookies(cookieUrl, cookiesExpression);
                    }
                }
                return CookieContainer ?? new CookieContainer();
            }
        }
    }
}
