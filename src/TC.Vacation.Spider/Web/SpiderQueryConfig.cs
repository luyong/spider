﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using HttpWebAdapters;

namespace TC.Vacation.Spider.Web
{
    internal class SpiderQueryConfig
    {
        #region ctor

        public SpiderQueryConfig()
        {

        }

        #endregion

        private Type _DynamicObjectType;

        /// <summary>
        /// 返回一个新的HttpWebRequest
        /// </summary>

        public IHttpWebRequestFactory WebRequestFactory
        {
            get
            {
                if (_WebRequestFactory == null)
                {
                    _WebRequestFactory = new HttpWebRequestFactory();
                }
                return _WebRequestFactory;
            }
        }
        private IHttpWebRequestFactory _WebRequestFactory;

        /// <summary>
        /// 获取或设置默认对象的动态类型。当JSON序列化未指定类型时
        /// </summary>

        public Type DynamicObjectType
        {
            get
            {
                return _DynamicObjectType;
            }
            set
            {
                if (value.GetInterfaces().Where(item =>
                    item == typeof(IDynamicMetaObjectProvider) ||
                    item == typeof(IDictionary<string, object>))
                    .Count() == 2)
                {
                    _DynamicObjectType = value;
                }
                else
                {
                    throw new ArgumentException("The DynamicObjectType must inherit IDynamicMetaObjectProvider and IDictionary<string,object>. Example: ExpandoObject, or the built-in JsObject.");
                }
            }
        }
    }
}
