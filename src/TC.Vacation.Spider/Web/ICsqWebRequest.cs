﻿
namespace TC.Vacation.Spider.Web
{
    /// <summary>
    /// 请求接口
    /// </summary>
    public interface ICsqWebRequest : ICsqWebRequestMetadata
    {
        /// <summary>
        /// 请求的URL
        /// </summary>
        string Url { get; }

        /// <summary>
        /// 标识请求是否完成
        /// </summary>
        bool Complete { get; }

        /// <summary>
        /// 请求完成后返回的唯一标识
        /// </summary>
        object Id { get; set; }

        /// <summary>
        /// 请求配置项
        /// </summary>
        ServerConfig Options { get; set; }
    }
}
