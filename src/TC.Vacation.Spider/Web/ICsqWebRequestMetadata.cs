﻿
namespace TC.Vacation.Spider.Web
{

    public interface ICsqWebRequestMetadata
    {
        /// <summary>
        /// 请求超时--秒
        /// </summary>
        int Timeout { get; set; }

        /// <summary>
        /// 浏览器标识
        /// </summary>
        string UserAgent { get; set; }
    }
}
