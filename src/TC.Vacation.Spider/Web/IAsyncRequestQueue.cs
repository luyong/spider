﻿using System.Collections.Generic;

namespace TC.Vacation.Spider.Web
{
    /// <summary>
    /// 一个异步请求队列--未实现
    /// </summary>
    public interface IAsyncRequestQueue : ICsqWebRequest
    {
        /// <summary>
        /// 新增一个异步请求
        /// </summary>
        /// <param name="url"></param>
        void AddRequest(string url);

        /// <summary>
        /// 响应结果集
        /// </summary>
        /// <returns></returns>
        IEnumerable<ICsqWebResponse> Results { get; }

        /// <summary>
        /// 请求状态
        /// </summary>
        RequestState State { get; }
    }
}
