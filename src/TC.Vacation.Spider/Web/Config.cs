﻿using System;
using HttpWebAdapters;

namespace TC.Vacation.Spider.Web
{
    /// <summary>
    /// 全局配置
    /// </summary>

    internal static class Config
    {
        static Config()
        {
            DefaultConfig = new SpiderQueryConfig();
        }

        private static SpiderQueryConfig DefaultConfig;

        /// <summary>
        /// 返回一个web请求工厂
        /// </summary>

        public static IHttpWebRequestFactory WebRequestFactory
        {
            get
            {
                return DefaultConfig.WebRequestFactory;
            }
        }


        public static Type DynamicObjectType
        {
            get
            {
                return DefaultConfig.DynamicObjectType;
            }
            set
            {
                DefaultConfig.DynamicObjectType = value;
            }
        }
    }
}
