﻿using System;
using System.Net;
using System.Text;

namespace TC.Vacation.Spider.Web
{
    /// <summary>
    /// 异步响应接口
    /// </summary>
    public interface ICsqWebResponse : ICsqWebRequest
    {
        /// <summary>
        /// 返回的HTML
        /// </summary>
        string Html { get; }

        /// <summary>
        /// 响应开始时间
        /// </summary>
        DateTime? Started { get; }

        /// <summary>
        /// 响应结束时间
        /// </summary>
        DateTime? Finished { get; }

        /// <summary>
        /// 抓取耗时
        /// </summary>
        /// <value>
        /// The spent.
        /// </value>
        double Spent { get; }

        /// <summary>
        /// 标识请求是否成功
        /// </summary>
        bool Success { get; }

        /// <summary>
        /// HTTP状态码
        /// </summary>
        int HttpStatus { get; }

        /// <summary>
        /// HTTP状态标识字符串
        /// </summary>
        string HttpStatusDescription { get; }

        /// <summary>
        /// 请求出错时的异常信息
        /// </summary>
        string Error { get; }

        /// <summary>
        /// 请求出错时的异常对象
        /// </summary>
        WebException WebException { get; }

        /// <summary>
        /// 响应编码
        /// </summary>
        Encoding ResponseEncoding { get; }

        /// <summary>
        /// Gets the web proxy.
        /// </summary>
        /// <value>
        /// The web proxy.
        /// </value>
        IWebProxy WebProxy { get; }

        /// <summary>
        /// Gets the cookie.
        /// </summary>
        /// <value>
        /// The cookie.
        /// </value>
        CookieContainer Cookie { get; }
    }
}
