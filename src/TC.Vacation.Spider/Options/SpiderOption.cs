﻿using System.Collections.Generic;
using System.Text;
using System.Web;
using TC.Vacation.Spider.Rules;

namespace TC.Vacation.Spider
{
    /// <summary>
    /// 爬虫配置
    /// </summary>
    public sealed class SpiderOption
    {
        private List<string> _initseeds;
        /// <summary>
        /// 初始化抓取地址
        /// </summary>
        public List<string> InitSeeds
        {
            get { return _initseeds; }
            set { _initseeds = value; }
        }

        private int _timeout = 1500;
        /// <summary>
        /// 抓取超时时间
        /// </summary>
        public int Timeout
        {
            get { return _timeout; }
            set { _timeout = value; }
        }

        private string _userAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 UBrowser/3.1.1644.34 Safari/537.36";
        /// <summary>
        /// Gets or sets the user agent.
        /// </summary>
        /// <value>
        /// The user agent.
        /// </value>
        public string UserAgent
        {
            get { return _userAgent; }
            set { _userAgent = value; }
        }

        private bool _useProxy = false;
        /// <summary>
        /// 是否使用代理
        /// </summary>
        public bool UseProxy
        {
            get { return _useProxy; }
            set { _useProxy = value; }
        }

        private string _proxyUserName = "";
        /// <summary>
        /// 当使用代理时，需要指定的用户名
        /// </summary>
        public string ProxyUserName
        {
            get { return _proxyUserName; }
            set { _proxyUserName = value; }
        }

        private string _proxyPassWord = "";
        /// <summary>
        /// 当使用代理时，需要指定的密码
        /// </summary>
        public string ProxyPassWord
        {
            get { return _proxyPassWord; }
            set { _proxyPassWord = value; }
        }

        private string _proxyHost = "";
        /// <summary>
        /// 当使用代理时，需要指定的Host地址
        /// </summary>
        public string ProxyHost
        {
            get
            {
                if (string.IsNullOrEmpty(_proxyHost))
                {
                    if (Environmental == 1) //线下
                    {
                        _proxyHost = ProxyAddress.Local;
                    }
                    else if (Environmental == 2)
                    {
                        _proxyHost = ProxyAddress.OnLine;
                    }
                }
                return _proxyHost;
            }
            set { _proxyHost = value; }
        }

        private int _proxyPort = 0;
        /// <summary>
        /// 当使用代理时，需要指定的端口，默认为9999
        /// </summary>
        public int ProxyPort
        {
            get
            {
                if ((Environmental == 1 || Environmental == 2) && _proxyPort == 0)
                {
                    _proxyPort = ProxyAddress.Port;
                }
                return _proxyPort;
            }
            set { _proxyPort = value; }
        }

        /// <summary>
        /// html分析规则
        /// </summary>
        public RulesAnalyzer Analyzer { get; set; }

        private bool _async = false;
        /// <summary>
        /// 是否异步请求
        /// </summary>
        public bool Async
        {
            get { return _async; }
            set { _async = value; }
        }

        private bool _keepalive = false;

        /// <summary>
        /// Gets or sets a value indicating whether [keep alive].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [keep alive]; otherwise, <c>false</c>.
        /// </value>
        public bool KeepAlive
        {
            get { return _keepalive; }
            set { _keepalive = value; }
        }

        private Encoding _encoding = Encoding.UTF8;
        /// <summary>
        /// Gets or sets the encoding.
        /// </summary>
        /// <value>
        /// The encoding.
        /// </value>
        public Encoding Encoding
        {
            get { return _encoding; }
            set { _encoding = value; }
        }

        private string _method = "GET";
        /// <summary>
        /// Gets or sets the mehtod.
        /// </summary>
        /// <value>
        /// The mehtod.
        /// </value>
        public string Method
        {
            get { return _method; }
            set { _method = value; }
        }

        private string _contentType = "application/json";
        /// <summary>
        /// Gets or sets the type of the content.
        /// </summary>
        /// <value>
        /// The type of the content.
        /// </value>
        public string ContentType
        {
            get { return _contentType; }
            set { _contentType = value; }
        }

        private int _environmental = 1;
        /// <summary>
        /// 插件环境。线上、线下
        /// </summary>
        /// <value>
        /// The environmental.
        /// </value>
        public int Environmental
        {
            get { return _environmental; }
            set { _environmental = value; }
        }

        /// <summary>
        /// 插件名称
        /// </summary>
        public string PluginName
        {
            get;
            set;
        }
    }

    /// <summary>
    /// 自有代理地址、端口
    /// </summary>
    internal sealed class ProxyAddress
    {
        internal const string Local = "172.16.58.188";
        internal const string OnLine = "222.92.187.51";
        internal const int Port = 8888;
    }
}
