﻿using System;
using TC.Vacation.Spider.Core.Iocp;

namespace TC.Vacation.Spider.Core.Distributed
{
    public class Slave : TcpClient, IConection
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Slave"/> class.
        /// </summary>
        /// <param name="remoteIp">远程服务器IP地址</param>
        /// <param name="port">端口</param>
        public Slave(string remoteIp, int port):base(remoteIp,port)
        {

        }
    }
}
