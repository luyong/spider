﻿using System;
using TC.Vacation.Spider.Core.Iocp;

namespace TC.Vacation.Spider.Core.Distributed
{
    public class Master : TcpServer, IConection
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Master"/> class.
        /// </summary>
        /// <param name="connections">The connections.</param>
        /// <param name="buffersize">The buffersize.</param>
        public Master(int connections = 1000, int buffersize = 64) : base(connections, buffersize)
        {
            base.Init();
        }
    }
}
