﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TC.Vacation.Spider.Core.Storage
{
    public interface IStorage
    {
        void Save();

        object Get { get; set; }
    }
}
