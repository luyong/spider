﻿using System.Net.Sockets;

namespace TC.Vacation.Spider.Core.Iocp
{
    internal class AsyncUserToken
    {
        internal AsyncUserToken()
            : this(null)
        {

        }

        internal AsyncUserToken(Socket socket)
        {
            Socket = socket;
        }

        internal Socket Socket { get; set; }
    }
}
