﻿
namespace TC.Vacation.Spider.Rules
{
    public interface IAnalyzer
    {
        dynamic Filter(string html,string url);
    }
}
