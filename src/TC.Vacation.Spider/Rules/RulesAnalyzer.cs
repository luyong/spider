﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Web.Script.Serialization;
using HttpWebAdapters;
using TC.Vacation.Spider.Core;
using TC.Vacation.Spider.Events;
using TC.Vacation.Spider.Web;

namespace TC.Vacation.Spider.Rules
{
    /// <summary>
    /// 实现自定义规则的抽象基类
    /// </summary>
    public abstract class RulesAnalyzer : IAnalyzer, ICloneable
    {
        /// <summary>
        /// 从配置文件获取组件运行环境
        /// </summary>
        /// <value>
        /// The environmental.
        /// </value>
        public virtual int Environmental
        {
            get
            {
                string environmental = ConfigurationManager.AppSettings["Environmental"] ?? "";
                return string.IsNullOrEmpty(environmental) ? 0 : Convert.ToInt32(environmental);
            }
        }

        /// <summary>
        /// 创建副本--浅表副本
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            return this.MemberwiseClone();
        }

        /// <summary>
        /// Filters the specified HTML.
        /// </summary>
        /// <param name="html">The HTML.</param>
        public abstract dynamic Filter(string html, string url);
    }
}
