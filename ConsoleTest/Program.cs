﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Dynamic;
using System.Text;
using TC.Vacation.Spider;
using TC.Vacation.Spider.Rules;
using HtmlAgilityPack;

namespace ConsoleTest
{
    class Program
    {
        static void Main(string[] args)
        {
            const string baseUrl = @"http://www.shousibaocai.com/?s=设计模式";
            var option = new SpiderOption();
            Init(out option);
            option.InitSeeds = new List<string> { baseUrl };

            var spider = new Spider(option);

            spider.Start(x =>
            {
                dynamic list = x.DynamicEntity as List<dynamic>;
                foreach (var item in list)
                {
                    Console.WriteLine("name:{0}，href{1}",item.Name,item.Href);
                }
            },
            p =>
            {
                Console.WriteLine(p.Exception.ToString());
            });

            Console.ReadKey();
        }

        static void Init(out SpiderOption option)
        {
            option = new SpiderOption
            {
                Method = "get",
                Async = true,
                UseProxy = true,
                Analyzer = new FilterTorrent()
            };
        }

        public class FilterTorrent : RulesAnalyzer
        {
            public override dynamic Filter(string html, string url)
            {
                Dictionary<int, List<dynamic>> pages = new Dictionary<int, List<dynamic>>();
                var list = new List<dynamic>();
                var document = new HtmlDocument();
                document.LoadHtml(html);

                var nodes = document.DocumentNode.SelectNodes("//td[@class='x-item']");
                foreach (var item in nodes)
                {
                    dynamic entity = new ExpandoObject();
                    string name = item.SelectSingleNode("div/a").Attributes["title"].Value;
                    string href = item.SelectSingleNode("div[@class='tail']/a").Attributes["href"].Value;
                    entity.Name = name;
                    entity.Href = href;
                    list.Add(entity);
                }

                return list;
            }
        }
    }
}
